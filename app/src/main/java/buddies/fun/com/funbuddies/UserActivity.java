package buddies.fun.com.funbuddies;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

import buddies.fun.com.funbuddies.model.UserRealm;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by User on 20/05/2017.
 */

public class UserActivity extends AppCompatActivity {

    @BindView(R.id.tvFName)TextView tvFName;
    @BindView(R.id.tvUserName)TextView tvUserName;
    @BindView(R.id.tvLocation)TextView tvLocaton;
    @BindView(R.id.tvBday)TextView tvBday;
    @BindView(R.id.cvUserPic)CircleImageView cvUserPic;
    @BindView(R.id.tvUpdate)TextView tvUpdate;
    @BindView(R.id.tvAdd)TextView tvAdd;
    @BindView(R.id.userCover)ImageView userCover;

    int userId;
    private Realm realm;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_activity);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        realm = Realm.getDefaultInstance();
        Bundle b = getIntent().getExtras();
        userId = b.getInt("userId");
        Log.e("updateid", String.valueOf(userId));
        init(userId);
    }


    public void init(final int id){

        RealmQuery<UserRealm> query = realm.where(UserRealm.class);
        UserRealm result = query.equalTo("id", userId).findFirst();
        tvFName.setText(result.getFirstName()+" "+result.getLastName());
        tvUserName.setText("@"+result.getUserName());
        tvLocaton.setText(result.getLocation());
        Glide.with(UserActivity.this).load(result.getImage()).into(cvUserPic);

        tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("userIdAc", String.valueOf(userId)+" "+String.valueOf(id));
                Intent i = new Intent (UserActivity.this, UpdateAccount.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("userId",userId);
                startActivity(i);
            }
        });

        if(result.getCoverPhoto()==null){
            Glide.with(UserActivity.this).load(R.drawable.bg1).into(userCover);
        }else{
            Glide.with(UserActivity.this).load(result.getCoverPhoto()).into(userCover);
        }

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String bday = result.getBday();
        try {
            DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US);
            String cbday = dateFormat.format(df.parse(bday));
            tvBday.setText("Born on "+ cbday);
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

package buddies.fun.com.funbuddies;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by User on 19/05/2017.
 */

public class UtilityPreferences {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;

    private static final String PREF_NAME = "FunBuddiesPref";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_ISLOGGEDIN = "isLoggedIn";

    public UtilityPreferences(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        editor = sharedPreferences.edit();
    }

    public void setIsLoggedIn(Boolean isLoggedIn) {
        editor.putBoolean("isLoggedIn", isLoggedIn);
        editor.commit();
        editor.apply();
    }

    public boolean getIsLoggedIn() {
        return sharedPreferences.getBoolean("isLoggedIn", false);
    }

    public void logout(){
        editor.putBoolean(KEY_ISLOGGEDIN,false);
        editor.commit();
    }

    public void setFirstNamePreference(String firstName) {
        editor.putString("firstName", firstName);
        editor.commit();
        editor.apply();
    }

    public String getFirstNamePreference(){
        return sharedPreferences.getString("firstName", "");
    }

    public void setLastNamePreference(String lastName) {
        editor.putString("lastName", lastName);
        editor.commit();
        editor.apply();
    }

    public String getLastNamePreference(){
        return sharedPreferences.getString("lastName", "");
    }

    public void setUserNamePreference(String userName) {
        editor.putString("userName", userName);
        editor.commit();
        editor.apply();
    }

    public String getUserNamePreference(){
        return sharedPreferences.getString("userName", "");
    }


    public void setPicPreference(String userPic) {
        editor.putString("userPic", userPic);
        editor.commit();
        editor.apply();
    }

    public String getPicPreference(){
        return sharedPreferences.getString("userPic", "");
    }

    public void setIdPreference(int userId) {
        editor.putInt("userId", userId);
        editor.commit();
        editor.apply();
    }

    public int getIdPreference(){
        return sharedPreferences.getInt("userId", 0);
    }

    public void setIsProfilePicturePreference(Boolean isProfile) {
        editor.putBoolean("isProfile", isProfile);
        editor.commit();
        editor.apply();
    }

    public boolean getIsProfilePicturePreference() {
        return sharedPreferences.getBoolean("isProfile", false);
    }
}

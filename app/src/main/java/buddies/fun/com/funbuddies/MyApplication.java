package buddies.fun.com.funbuddies;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by User on 19/05/2017.
 */

public class MyApplication extends Application {

    private static MyApplication mInstance;

        @Override
        public void onCreate(){
            super.onCreate();
            Realm.init(this);
            RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                    .name(Realm.DEFAULT_REALM_NAME)
                    .schemaVersion(0)
                    .deleteRealmIfMigrationNeeded()
                    .build();
            Realm.setDefaultConfiguration(realmConfiguration);

            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Lato-Light.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );

    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }
}

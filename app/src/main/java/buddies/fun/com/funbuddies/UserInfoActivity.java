package buddies.fun.com.funbuddies;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import buddies.fun.com.funbuddies.model.UserRealm;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmResults;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by User on 22/05/2017.
 */

public class UserInfoActivity extends AppCompatActivity {

    @BindView(R.id.tvFName)TextView tvFName;
    @BindView(R.id.tvUserName)TextView tvUserName;
    @BindView(R.id.tvLocation)TextView tvLocaton;
    @BindView(R.id.tvBday)TextView tvBday;
    @BindView(R.id.cvUserPic)CircleImageView cvUserPic;
    @BindView(R.id.tvUpdate)TextView tvUpdate;
    @BindView(R.id.tvAdd)TextView tvAdd;
    @BindView(R.id.userCover)ImageView userCover;

    int userId;
    private Realm realm;
    UserRealm userRealm;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_activity);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        realm = Realm.getDefaultInstance();
        Bundle b = getIntent().getExtras();
        userId = b.getInt("user");
        Log.e("userId", String.valueOf(userId));
        init(userId);
    }


    public void init(int userId){
        tvUpdate.setVisibility(View.INVISIBLE);
        tvAdd.setVisibility(View.INVISIBLE);
        final RealmResults<UserRealm> resultUser = realm.where(UserRealm.class).findAll();
        userRealm = resultUser.get(userId);
        Log.e("UserA", userRealm.getUserName()+" "+userRealm.getImage());
        tvFName.setText(userRealm.getFirstName()+" "+userRealm.getLastName());
        tvUserName.setText("@"+userRealm.getUserName());
        tvLocaton.setText(userRealm.getLocation());

        if(userRealm.getCoverPhoto()==null){
            Glide.with(UserInfoActivity.this).load(R.drawable.bg1).into(userCover);
        }else{
            Glide.with(UserInfoActivity.this).load(userRealm.getCoverPhoto()).into(userCover);
        }

        Glide.with(UserInfoActivity.this).load(userRealm.getImage()).into(cvUserPic);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String bday = userRealm.getBday();
        try {
            DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US);
            String cbday = dateFormat.format(df.parse(bday));
            tvBday.setText("Born on "+ cbday);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


}

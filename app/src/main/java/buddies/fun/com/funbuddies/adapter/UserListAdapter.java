package buddies.fun.com.funbuddies.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import buddies.fun.com.funbuddies.R;
import buddies.fun.com.funbuddies.UserActivity;
import buddies.fun.com.funbuddies.UserInfoActivity;
import buddies.fun.com.funbuddies.model.UserRealm;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by User on 20/05/2017.
 */

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.MyViewHolder> {

    private Context mContext;
    private List<UserRealm> userRealmList;
    ArrayList<UserRealm> arraylist;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView userName, name, freq;
        public CircleImageView userPic;
        public CardView cardView;

        public MyViewHolder(View view) {
            super(view);
//            uPOst = (TextView) view.findViewById(R.id.uPost);
            userName = (TextView) view.findViewById(R.id.userName);
            userPic = (CircleImageView) view.findViewById(R.id.userPic);
            cardView = (CardView) view.findViewById(R.id.cardView);
        }
    }

    public UserListAdapter(Context mContext, List<UserRealm> userRealmList) {
        this.mContext = mContext;
        this.userRealmList = userRealmList;
        arraylist = new ArrayList<UserRealm>();
        arraylist.addAll(userRealmList);
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final UserRealm uRealm = userRealmList.get(position);
//        holder.name.setText(merchant.getFirstName()+ " "+ merchant.getLastName());
        holder.userName.setText("@"+uRealm.getUserName());
        Glide.with(mContext).load(uRealm.getImage()).into(holder.userPic);
        Log.e("USER", position+" "+uRealm.toString()+" "+uRealm.getFirstName());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent (mContext, UserInfoActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("user",position);
                mContext.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return userRealmList.size();
    }

    public void filter(String charText) {
        Log.e("SearchAdapter", charText);

        charText = charText.toLowerCase(Locale.getDefault());

        userRealmList.clear();
        if (charText.length() == 0) {
            userRealmList.addAll(arraylist);
            Log.e("SearchAdapter0", String.valueOf(arraylist));
        } else {
            Log.e("SearchAdapter!", String.valueOf(arraylist));
            for (UserRealm postDetail : arraylist) {
                if (charText.length() != 0 && postDetail.getUserName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    userRealmList.add(postDetail);
                } else if (charText.length() != 0 && postDetail.getFirstName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    userRealmList.add(postDetail);
                }
            }
        }
        notifyDataSetChanged();
    }
}

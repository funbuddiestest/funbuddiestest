package buddies.fun.com.funbuddies;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import buddies.fun.com.funbuddies.model.UserRealm;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    @BindView(R.id.register)TextView regsiter;
    @BindView(R.id.login)TextView login;
    @BindView(R.id.username)EditText username;
    @BindView(R.id.password)EditText password;
    @BindView(R.id.regUName)EditText regUName;
    @BindView(R.id.regFName)EditText regFName;
    @BindView(R.id.regLastName)EditText regLastName;
    @BindView(R.id.regBday)EditText regBday;
    @BindView(R.id.regLocation)EditText regLocation;
    @BindView(R.id.regPic)CircleImageView regPic;
    @BindView(R.id.regPassword)EditText regPassword;
    @BindView(R.id.linearLogin)LinearLayout linearLogin;
    @BindView(R.id.linearRegister)LinearLayout linearRegister;
    @BindView(R.id.btnLogin)Button btnLogin;
    @BindView(R.id.btnRegsiter)Button btnRegsiter;

    private Realm realm;
    private int idUser;
    Uri outputFileUri = null;
    File photoFile;

    Uri selectedImageUri;
    UtilityPreferences utilityPreferences;
    String[] location;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        realm = Realm.getDefaultInstance();
        init();
    }

    public void init(){
        utilityPreferences = new UtilityPreferences(getApplicationContext());
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(username.getText().length()==0 ||
                        password.getText().length()==0){
                    Toast.makeText(LoginActivity.this, "fill all fields!", Toast.LENGTH_SHORT).show();
                }else{
                    validatelogin(username.getText().toString(),password.getText().toString());
                }
            }
        });

        btnRegsiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(regUName.getText().length()==0 ||
                        regPassword.getText().length() ==0 ||
                        regFName.getText().length() ==0 ||
                        regLastName.getText().length() == 0 ||
                        regBday.getText().length() == 0 ||
                        regLocation.getText().length() == 0 ||
                        selectedImageUri == null){
                    MaterialDialog dialog = new MaterialDialog.Builder(LoginActivity.this)
                            .typeface("Lato-Medium.ttf", "Lato-Light.ttf")
                            .content("Please fill in all fields")
                            .positiveText("OK")
                            .show();
                }else if(isUsernameExist(regUName.getText().toString())) {
                    regUName.setText("");
                    regPassword.setText("");
                    MaterialDialog dialog = new MaterialDialog.Builder(LoginActivity.this)
                            .typeface("Lato-Medium.ttf", "Lato-Light.ttf")
                            .content("user name already used!")
                            .positiveText("OK")
                            .show();
                } else if(regPassword.getText().length()<8){
                    MaterialDialog dialog = new MaterialDialog.Builder(LoginActivity.this)
                            .typeface("Lato-Medium.ttf", "Lato-Light.ttf")
                            .content("password must be atleast 8 characters")
                            .positiveText("OK")
                            .show();
                }else {
                    RealmResults<UserRealm> resultUser = realm.where(UserRealm.class).findAll();
                    if(resultUser.size()>0 ){
                        UserRealm exist = resultUser.get(resultUser.size()-1);
                        idUser = exist.getId()+1;
                        registerProccess(idUser);
                    }else{
                        idUser = 1;
                        Log.d("userID",String.valueOf(idUser));
                        registerProccess(idUser);
                    }
                }
            }
        });

        regLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                location = getResources().getStringArray(R.array.location);
                new MaterialDialog.Builder(LoginActivity.this)
                        .typeface("Lato-Medium.ttf", "Lato-Light.ttf")
                        .title("Select")
                        .items(R.array.location)
                        .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                regLocation.setText(location[which]);
                                return true;
                            }
                        })
                        .positiveText("OK")
                        .show();
            }
        });

        regBday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        LoginActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAccentColor(Color.parseColor("#33578c"));
                dpd.setTitle("Birthday");
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });


        regPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImageIntent();
            }
        });

        regsiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearRegister.setVisibility(View.VISIBLE);
                linearLogin.setVisibility(View.GONE);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearLogin.setVisibility(View.VISIBLE);
                linearRegister.setVisibility(View.GONE);
            }
        });

    }

    private void validatelogin(String user, String pWord){
        Realm realm = Realm.getDefaultInstance();
        //check username
        RealmResults listUsername = realm
                .where(UserRealm.class)
                .equalTo("userName",user).findAll();

        if(listUsername.size()>0){
            RealmResults listPass = realm
                    .where(UserRealm.class)
                    .equalTo("userName",user)
                    .equalTo("password",pWord)
                    .findAll();
            if(listPass.size()>0){
                RealmQuery<UserRealm> query = realm.where(UserRealm.class);
                UserRealm result = query.equalTo("userName", user).findFirst();

                utilityPreferences.setIdPreference(result.getId());
                utilityPreferences.setFirstNamePreference(result.getFirstName());
                utilityPreferences.setLastNamePreference(result.getLastName());
                utilityPreferences.setUserNamePreference(result.getUserName());
                utilityPreferences.setPicPreference(result.getImage());
                utilityPreferences.setIsLoggedIn(true);
                Intent main = new Intent(LoginActivity.this, MainActivity.class);
                main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(main);
                LoginActivity.this.finish();
            }else{
                username.setText("");
                password.setText("");
                MaterialDialog dialog = new MaterialDialog.Builder(LoginActivity.this)
                        .typeface("Lato-Medium.ttf", "Lato-Light.ttf")
                        .content("Username & password does not match!")
                        .positiveText("OK")
                        .show();
            }
        }else{
            username.setText("");
            password.setText("");
            MaterialDialog dialog = new MaterialDialog.Builder(LoginActivity.this)
                    .typeface("Lato-Medium.ttf", "Lato-Light.ttf")
                    .content("Username not found")
                    .positiveText("OK")
                    .show();
        }
    }

    private void registerProccess(int id){
        realm.beginTransaction();
        UserRealm user = realm.createObject(UserRealm.class);
        user.setId(id);
        user.setFirstName(regFName.getText().toString());
        user.setLastName(regLastName.getText().toString());
        user.setUserName(regUName.getText().toString());
        user.setPassword(regPassword.getText().toString());
        user.setLocation(regLocation.getText().toString());
        user.setBday(regBday.getText().toString());
        user.setImage(selectedImageUri.toString());
        Log.e("validateLogin", username.getText().toString() + " "+password.getText().toString());
        realm.commitTransaction();
        MaterialDialog dialog = new MaterialDialog.Builder(LoginActivity.this)
                .typeface("Lato-Medium.ttf", "Lato-Light.ttf")
                .title("Congratulations")
                .content("You have successfully registered.")
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        validatelogin(regUName.getText().toString(),regPassword.getText().toString());
                    }
                })
                .show();

    }

    private boolean isUsernameExist(String username){
        RealmResults listUsername = realm
                .where(UserRealm.class)
                .equalTo("userName",username).findAll();
        if(listUsername.size()>1){
            return true;
        }else{
            return false;
        }
    }

    public void openImageIntent() {
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator +
                "FunBuddies" + File.separator);
        root.mkdir();
        final String fname = "FunBuddies_" + System.currentTimeMillis() + ".jpg";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);

        checkMediaPermisssion();
        // Camera
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        //FileSystem(Gallery)
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");
        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
        startActivityForResult(chooserIntent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 0) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }


                if (isCamera) {
                    try {
                        selectedImageUri = outputFileUri;
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        // downsizing image as it throws OutOfMemory Exception for larger
                        options.inSampleSize = 1;
                        final Bitmap bitmap = BitmapFactory.decodeFile(selectedImageUri.getPath(), options);
                        ImageFileHelper.checkOrientation(selectedImageUri.getPath(), bitmap);
                        Glide.with(this).load(selectedImageUri).into(regPic);
                        photoFile = new File(selectedImageUri.getPath());
                        FileOutputStream fos = new FileOutputStream(photoFile);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);
                        fos.close();

                        Log.e("Image", selectedImageUri.toString()+" "+photoFile);

                    } catch (Exception e) {
                        Log.e("error camera", e.toString());
                        Toast.makeText(getApplicationContext(), "Failed to Capture image. Please try again",
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    selectedImageUri = data == null ? null : data.getData();

                    try {

                        String realPath = null;

                        realPath = ImageFileHelper.getPath(this, selectedImageUri);
                        photoFile = new File(realPath);
                        InputStream input = getContentResolver().openInputStream(selectedImageUri);
                        final Bitmap bitmap = BitmapFactory.decodeStream(input);


                        Glide.with(this).load(selectedImageUri).into(regPic);
                        FileOutputStream fos = new FileOutputStream(photoFile);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);
                        fos.close();

                        Log.e("Imaget", selectedImageUri.toString()+" "+photoFile);
//                        utilityPreference.setAvatarPreference(selectedImageUri.toString());
                        ProgressDialog pd = new ProgressDialog(LoginActivity.this);
                        pd.setMessage("uploading..."); pd.show();
//                        generateMultipart(photoFile);

                        pd.dismiss();

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (resultCode == RESULT_CANCELED) {
//            Toast.makeText(getApplicationContext(),
//                    "User cancelled", Toast.LENGTH_SHORT)
//                    .show();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    public static final int MY_PERMISSIONS_REQUEST_CODE= 99;
    public boolean checkMediaPermisssion(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_CODE);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_CODE);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_GRANTED) {
                    }

                } else {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year+"/"+(++monthOfYear)+"/"+dayOfMonth;
        regBday.setText(date);
    }
}

package buddies.fun.com.funbuddies;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import buddies.fun.com.funbuddies.model.UserRealm;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by User on 21/05/2017.
 */

public class UpdateAccount extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    @BindView(R.id.updateFName)EditText updateFName;
    @BindView(R.id.updateLName)EditText updateLName;
    @BindView(R.id.updateUserName)EditText updateUserName;
    @BindView(R.id.updateLocation)EditText updateLocation;
    @BindView(R.id.updateBirthday)EditText updateBirthday;
    @BindView(R.id.updatePic)CircleImageView updatePic;
    @BindView(R.id.btnUpdate)Button btnUpdate;
    @BindView(R.id.updateCover)ImageView updateCover;

    private Realm realm;
    Uri outputFileUri = null;
    File photoFile;

    Uri selectedImageUri, selectedCoverUri;
    int id;
    UtilityPreferences utilityPreferences;
    String[] location;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_activity);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        realm = Realm.getDefaultInstance();
        utilityPreferences = new UtilityPreferences(getApplication());
        Bundle b = getIntent().getExtras();
        id = b.getInt("userId");
        Log.e("updateid", String.valueOf(id));
        getUserDetails(id);

    }


    public void getUserDetails(final int userId){
        RealmQuery<UserRealm> query = realm.where(UserRealm.class);
        UserRealm result = query.equalTo("id", userId).findFirst();
        Log.e("userUpdate", result.getUserName());
        Glide.with(UpdateAccount.this).load(result.getImage()).into(updatePic);
        updateFName.setText(result.getFirstName());
        updateLName.setText(result.getLastName());
        updateUserName.setText(result.getUserName());
        updateLocation.setText(result.getLocation());
        updateBirthday.setText(result.getBday());

        if(result.getCoverPhoto()==null){
            Glide.with(UpdateAccount.this).load(R.drawable.bg1).into(updateCover);
        }else{
            Glide.with(UpdateAccount.this).load(result.getCoverPhoto()).into(updateCover);
        }

        updateLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                location = getResources().getStringArray(R.array.location);
                new MaterialDialog.Builder(UpdateAccount.this)
                        .typeface("Lato-Medium.ttf", "Lato-Light.ttf")
                        .title("Select")
                        .items(R.array.location)
                        .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                updateLocation.setText(location[which]);
                                return true;
                            }
                        })
                        .positiveText("OK")
                        .show();
            }
        });

        updateBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        UpdateAccount.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAccentColor(Color.parseColor("#33578c"));
                dpd.setTitle("Birthday");
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        updatePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                utilityPreferences.setIsProfilePicturePreference(true);
                openImageIntent();
            }
        });

        updateCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                utilityPreferences.setIsProfilePicturePreference(false);
                openImageIntent();
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(updateFName.getText().length()==0 ||
                        updateUserName.getText().length()==0 ||
                        updateLName.getText().length()==0||
                        selectedImageUri == null){
                    Toast.makeText(UpdateAccount.this, "please fill all fields!", Toast.LENGTH_SHORT).show();
                }else{
                    if(isUsernameExist(updateUserName.getText().toString())){
                        Toast.makeText(UpdateAccount.this, "User Name alredy Used!", Toast.LENGTH_SHORT).show();
                    }else{
                        updateData(updateFName.getText().toString(),updateUserName.getText().toString(),
                                updateLName.getText().toString(),userId);
                        MaterialDialog dialog = new MaterialDialog.Builder(UpdateAccount.this)
                                .typeface("Lato-Medium.ttf", "Lato-Light.ttf")
                                .content("Your account has been updated")
                                .positiveText("OK")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        utilityPreferences.setUserNamePreference(updateUserName.getText().toString());
                                        utilityPreferences.setFirstNamePreference(updateFName.getText().toString());
                                        utilityPreferences.setLastNamePreference(updateLName.getText().toString());
                                        utilityPreferences.setPicPreference(selectedImageUri.toString());
                                        finish();
                                        Intent i = new Intent(UpdateAccount.this, MainActivity.class);
                                        startActivity(i);

                                    }
                                })
                                .show();
                    }
                }
            }
        });
    }

    private boolean isUsernameExist(String username){
        RealmResults listUsername = realm
                .where(UserRealm.class)
                .equalTo("userName",username).findAll();
        if(listUsername.size()>1){
            return true;
        }else{
            return false;
        }
    }

    private void updateData(String firstName, String username, String lastName, int id){
        UserRealm user = realm.where(UserRealm.class)
                .equalTo("id", id)
                .findFirst();
        realm.beginTransaction();
        user.setUserName(username);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setImage(selectedImageUri.toString());
        user.setCoverPhoto(selectedCoverUri.toString());
        realm.commitTransaction();
        onResume();
    }

    public void openImageIntent() {
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator +
                "FunBuddies" + File.separator);
        root.mkdir();
        final String fname = "FunBuddies_" + System.currentTimeMillis() + ".jpg";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);

        // Camera
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        //FileSystem(Gallery)
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");
        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
        startActivityForResult(chooserIntent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 0) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }

                if (isCamera) {

                    if(utilityPreferences.getIsProfilePicturePreference()==true){
                        try {
                            selectedImageUri = outputFileUri;
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            // downsizing image as it throws OutOfMemory Exception for larger
                            options.inSampleSize = 1;
                            final Bitmap bitmap = BitmapFactory.decodeFile(selectedImageUri.getPath(), options);
                            ImageFileHelper.checkOrientation(selectedImageUri.getPath(), bitmap);
                            Glide.with(this).load(selectedImageUri).into(updatePic);

                            photoFile = new File(selectedImageUri.getPath());
                            FileOutputStream fos = new FileOutputStream(photoFile);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);
                            fos.close();


                        } catch (Exception e) {
                            Log.e("error camera", e.toString());
                            Toast.makeText(getApplicationContext(), "Failed to Capture image. " +
                                    "Please try again", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        try {
                            selectedCoverUri = outputFileUri;
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            // downsizing image as it throws OutOfMemory Exception for larger
                            options.inSampleSize = 1;
                            final Bitmap bitmap = BitmapFactory.decodeFile(selectedCoverUri.getPath(), options);
                            ImageFileHelper.checkOrientation(selectedCoverUri.getPath(), bitmap);
                            Glide.with(this).load(selectedCoverUri).into(updateCover);

                            photoFile = new File(selectedCoverUri.getPath());
                            FileOutputStream fos = new FileOutputStream(photoFile);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);
                            fos.close();


                        } catch (Exception e) {
                            Log.e("error camera", e.toString());
                            Toast.makeText(getApplicationContext(), "Failed to Capture image. Please try again",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                } else {

                    if(utilityPreferences.getIsProfilePicturePreference()==true){
                        selectedImageUri = data == null ? null : data.getData();
                        try {
                            String realPath = null;

                            realPath = ImageFileHelper.getPath(this, selectedImageUri);
                            photoFile = new File(realPath);
                            InputStream input = getContentResolver().openInputStream(selectedImageUri);
                            final Bitmap bitmap = BitmapFactory.decodeStream(input);

                            Glide.with(this).load(selectedImageUri).into(updatePic);
                            FileOutputStream fos = new FileOutputStream(photoFile);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);
                            fos.close();

                            Log.e("Imaget", selectedImageUri.toString()+" "+photoFile);
                            ProgressDialog pd = new ProgressDialog(UpdateAccount.this);
                            pd.setMessage("uploading..."); pd.show();
                            pd.dismiss();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }else{
                        selectedCoverUri = data == null ? null : data.getData();

                        try {

                            String realPath = null;

                            realPath = ImageFileHelper.getPath(this, selectedCoverUri);
                            photoFile = new File(realPath);
                            InputStream input = getContentResolver().openInputStream(selectedCoverUri);
                            final Bitmap bitmap = BitmapFactory.decodeStream(input);


                            Glide.with(this).load(selectedCoverUri).into(updateCover);
                            FileOutputStream fos = new FileOutputStream(photoFile);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);
                            fos.close();

                            Log.e("Imaget", selectedCoverUri.toString()+" "+photoFile);
                            ProgressDialog pd = new ProgressDialog(UpdateAccount.this);
                            pd.setMessage("uploading..."); pd.show();
                            pd.dismiss();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
        } else if (resultCode == RESULT_CANCELED) {
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year+"/"+(++monthOfYear)+"/"+dayOfMonth;
        updateBirthday.setText(date);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
